local mergeTables = function(t1, t2)
	local t3 = t1;
	for k,v in pairs(t2) do t3[k] = v end
	return t3;
end
local profile = {};
local sets = {
    Idle = {
        Main = 'Barbaroi axe',
        Sub = 'Lantern Shield',
        Ammo = 'Pet Fd. Gamma',
        Head = 'Shep. bonnet',
        Neck = 'Fang Necklace',
        Ear1 = 'Beetle Earring +1',
        Ear2 = 'Beetle Earring +1',
        Body = 'Alumine haubert',
        Hands = 'Ryl.Sqr. Mufflers',
        Ring1 = 'Deft Ring',
        Ring2 = 'Deft Ring',
        Back = 'Safety Mantle',
        Waist = 'Life Belt',
        Legs = 'Bastokan Subligar',
        Feet = 'Greaves',
    },
    Reward = {
        Ammo = 'Pet Fd. Gamma',
    },
	CallBeast = {},
};

profile.jugTable = 
{
	Sheep = {
		Ready = 'Lamb Chop',
		Set = {
			Ammo = 'S. Herbal Broth',
		},
	},
	Tiger = {
		Ready = 'Razor Fang',
		Set = {
			Ammo = 'Meat Broth',
		},
	},
	Crab = {
		Ready = 'Big Scissors',
		Set = {
			Ammo = 'Fish Oil Broth',
		},
	},
	Lizard = {
		Ready = 'Brain Crush',
		Set = {
			Ammo = 'Carrion Broth',
		},
	},
	Bug = {
		Ready = 'Mandibular Bite',
		Set = {
			Ammo = 'Antica Broth',
		},
	},
};

profile.Sets = sets;

profile.jug = 'Sheep';

profile.Packer = {
};

profile.OnLoad = function()
    gSettings.AllowAddSet = true;
	for k,v in pairs(profile.jugTable) do
		AshitaCore:GetChatManager():QueueCommand(1, string.format('/alias /' .. k .. ' /lac exec gProfile.jug = "' .. k .. '";'));
	end
	AshitaCore:GetChatManager():QueueCommand(1, string.format('/alias /Ready /lac exec gProfile.DoReady();'));
end


profile.OnUnload = function()
	for k,v in pairs(profile.jugTable) do
		AshitaCore:GetChatManager():QueueCommand(1, string.format('/alias del /' .. k));
	end
	AshitaCore:GetChatManager():QueueCommand(1, string.format('/alias del /Ready'));
end

profile.HandleCommand = function(args)
end

profile.HandleDefault = function()
	local player = gData.GetPlayer();
	if (player.Status == 'Engaged') then
		gFunc.EquipSet(sets.Idle)
	else
		gFunc.EquipSet(sets.Idle)
	end
end

profile.HandleAbility = function()
	local ability = gData.GetAction();
	if string.match(ability.Name, 'Call Beast') or string.match(ability.Name, 'Bestial Loyalty') then
		gFunc.EquipSet(profile.jugTable[profile.jug].Set);
	elseif string.match(ability.Name, 'Reward') then
		gFunc.EquipSet(sets.Reward);
	end 
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
end

profile.DoReady = function()
	AshitaCore:GetChatManager():QueueCommand(1, string.format('/pet "%s" <t>', profile.jugTable[profile.jug].Ready));
end

return profile;